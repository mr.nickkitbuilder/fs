using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
public class multitable : MonoBehaviour
{
    // const variables
    const int Ways = 4;
    //

    // public variables 
    public Text[] answers; // (text of green buttons)
    public Text task; // (text above buttons)
    //

    //private values
    int a, b;
    //

    //Functions
    void Refresh()
    {
        a = UnityEngine.Random.Range(2, 10); // init a
        b = UnityEngine.Random.Range(2, 10); // init b
        task.text = a.ToString() + " x " + b.ToString(); // init task
        bool[] answersInitWays = new bool[Ways]; // We have different ways to init button values in this array, 
                                                 //we say what way, we will use
        int[] answersUnRandomPosition = new int[answers.Length]; // We need to have answers in random order,
                                                                 // do it later, but now let's save data here
        answersUnRandomPosition[0] = a * b; // first answer will be correct
        int trueAnswers = 0; // count of true answers to check the way, where count of true positions less than answers.Length
        int shift = 0;
        for (int i = 0; i < Ways; i++)
        {
            if(i == Ways - answers.Length + 1 + shift)
            {
                if (trueAnswers == shift)
                {
                    for(int j = i; j < Ways; j++)
                    {
                        answersInitWays[j] = true;
                    }
                    break;
                }
                shift++;
            }

            if(trueAnswers == answers.Length - 1)
            {
                answersInitWays[i] = false;
            }
            else
            {
                bool answer = Convert.ToBoolean(UnityEngine.Random.Range(0, 2));
                answersInitWays[i] = answer;
                trueAnswers += answer ? 1 : 0;
            }
        }
        int onePositionIndex = 0;
        for( int i = 1; i < answers.Length; i++)
        {
            for (int j = onePositionIndex; j < answersInitWays.Length; j++)
            {
                if (answersInitWays[j])
                {
                    onePositionIndex = j;
                    break;
                }
                else
                {
                    if(j == answersInitWays.Length - 1)
                    {
                        i = answers.Length;
                    }
                }
            }
            switch (onePositionIndex)
            {
                case 0:
                    answersUnRandomPosition[i] = (Math.Max(a, b) ) * (Math.Min(a, b) +1);
                    break;
                case 1:
                    answersUnRandomPosition[i] = (Math.Max(a,b) - 1)  * Math.Min(a,b);
                    break;
                case 2:
                    answersUnRandomPosition[i] = (Math.Max(a, b)) * (Math.Min(a, b) + 2);
                    break;
                case 3:
                    answersUnRandomPosition[i] = (Math.Max(a, b) - 2) * Math.Min(a, b);
                    break;
            }
            onePositionIndex++;
        }
        ArrayList answersList = new ArrayList(answersUnRandomPosition);
        for(int i = 0; i < answers.Length; i++)
        {
            int removingIndex = UnityEngine.Random.Range(0, answersList.Count);
            answers[i].text = answersList[removingIndex].ToString();
            answersList.RemoveAt(removingIndex);
        }

    }

    public void Check(int id)
    {
        if (Convert.ToInt32(answers[id].text) == a * b)
        {
            Refresh();
        }
    }
    //




    private void Start()
    {
        Refresh();
    }
}
